# citecd - всё из коробки!

Представляем citecd - готовая из коробки система автоматизации процесса разработки и каток для раскатки прода! Вы нажимаете кнопку, всё происходит само!

## Быстрый старт

Поймите Computer science, методологию и инструметарий DevOps и можно пользоваться.

### Системные требования

* Понадобятся минимум 5000 серверов минимум по 72 ядра и минимум 1ТБ ОЗУ.
* Клавиатура, монитор.

### Установка
setup.exe сразу установит все части системы на все ваши серверы.
```
chmod +x setup.exe

setup.exe
```
*Пока выполняется установка, я буду выполнять задания курса* [DevOps от Rebrain](https://my.rebrainme.com)


### Тесты

Как обычно в директории /tests/ и полностью автоматизированы.


## Авторы

**Sukhochev A.E Thompson** - *автор и владелец этого приватного репозитория*

## Лицензия

MIT License - see the [LICENSE.md](LICENSE.md) file for details

